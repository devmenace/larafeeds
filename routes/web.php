<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

//Route::resource('/users', 'UsersController')->middleware('auth');
//Route::resource('/feeds', 'FeedController')->middleware('auth');

//Route::group(['middleware' => 'auth'], function () {
//    Route::prefix('/feeds')->group(function () {
//        Route::get('/', 'FeedController@index');
//        Route::get('/{feed}', 'FeedController@show');
//        Route::post('/', 'FeedController@create');
//        Route::put('/{feed}', 'FeedController@edit');
//        Route::delete('/{feed}', 'FeedController@edestroy');
//    });
//});


Route::group(['middleware' => 'auth'], function () {
        Route::resource('/users', 'UsersController');
        Route::resource('/feeds', 'FeedController');
});

