@extends('feeds.layout')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Check all Feeds</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('feeds.create') }}"> Create new feeds</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Title</th>
            <th>Body</th>
            <th width="250px">Action</th>
        </tr>
        @foreach ($feeds as $feed)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $feed->title }}</td>
                <td>{{ $feed->body }}</td>
                <td>
                    <form action="{{ route('feeds.destroy',$feed->id) }}" method="POST">

                        <a class="btn btn-info" href="{{ route('feeds.show',$feed->id) }}">Show</a>

                        <a class="btn btn-primary" href="{{ route('feeds.edit',$feed->id) }}">Edit</a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $feeds->links() !!}

@endsection
