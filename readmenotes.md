Running initial migration to create DB Tables by    
`php artisan migrate`   

Or delete existing tables and create them again with 0 records  
` php artisan migrate:fresh`

Seed database with Demo user and 5 feed records by running  
`
php artisan db:seed
`

Now you should have user     
---
Username`Demo`  
Email `demo@demo.com`   
Password `demo`

You can change credentials for user in  
`database/seeds/UserTableSeeder.php`    
Allso you can modify feeds data in  
`database/seeds/FeedTableSeeder.php`
