<?php

use Illuminate\Database\Seeder;
//use DB;

class FeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('feeds')->truncate();

        $feeds = [];

        foreach (range(1, 5) as $index) {
            $feeds[]=[
                'title' => "Feed Title $index",
                'body' => "Feed $index body bla bla bla",
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        DB::table('feeds')->insert($feeds);
    }
}
